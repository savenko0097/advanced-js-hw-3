## деструктуризация - 

нужна чтоб из массива или объекта извлекать значения


ВМЕСТО
const person = {
  name: 'Anna',
  age: 30,
  city: 'Lviv'
};

console,log(person.name, person.age)


НАПИСАТЬ:

const {firstname: name, age}  = person

console.log(firstname, age)